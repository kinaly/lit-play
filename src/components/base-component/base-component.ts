import {LitElement, html, css, unsafeCSS} from 'lit';
import {customElement} from 'lit/decorators.js';

import baseComponentStyles from './base-component-styles.less';

@customElement('base-component')
class BaseComponent extends LitElement {
	static get styles() {
    return css`${unsafeCSS(baseComponentStyles)}`
  }

	render() {
		return html`
			<p>Hello world!</p>
		`;
	}
}