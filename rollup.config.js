import typescript from '@rollup/plugin-typescript';
import lessModules from 'rollup-plugin-less-modules';
import resolve from '@rollup/plugin-node-resolve';

const typeScriptOptions = {
  target: 'es6',
  noResolve: true,
  experimentalDecorators: true
}

export default [{
  input: {
    'base-component': 'src/components/base-component/base-component.ts'
  },
  output: {
    dir: 'dist/components',
    format: 'esm'
  },
  plugins: [
    lessModules(),
    typescript(typeScriptOptions),
    resolve()
    ]
}];

