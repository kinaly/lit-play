# Starter Nunjucks Static Site with Lit Elements

## Getting started
`npm install`

To build the site, run:
`npm run build`

To serve the site locally, watch the site files, and re-build automatically, run:
`npm run serve`

The site will usually be served at http://localhost:8000.

## Web Components
We are using Lit to write components and Rollup to build them.

### Create a component
* Create a folder named after the component in `src/components`
* Inside the folder create a file named <component-name>.js
* In `rollup.config.js` add the component to the input array

### Add a style module based on a less file
Inside the component folder, create a file named <component-name>-styles.less

To import the styles, amend <component-name>.js as follows:

* Make sure to import `unsafeCSS` module from Lit
`import {LitElement, html, css, unsafeCSS} from 'lit';`
* Add `import componentNameStyles from './<component-name>-styles.less';`
* Inside the class declaration, add:
```javascript
static get styles() {
  return css`${unsafeCSS(componentNameStyles)}`
}  
```

### Building components
`npm run build-components`

### Using the components
In the `<head>` of the page add the following line
`<script type="module" src="./components/component-name.js"></script>`

Then in your HTML file
`<component-name></component-name>`

## Static Site
We are using Nunjucks to serve static HTML page

### Building the site
`npm run build-html`

### Building styles
We are using LESS for writing CSS

`npm run build-styles`

## TODO
* Create a script that can bootstrap a component checking all the points in "Adding a web component"
